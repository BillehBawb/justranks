package com.billehbawb.justranks.commands;

import com.billehbawb.justranks.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class RankRevoke implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            return false;
        }

        Player player = (Player) sender;

        if (args.length != 2) {
            player.sendMessage(ChatColor.RED + "Incorrect usage! Try '/rankrevoke <PlayerName> <RankName>'");
            return false;
        }
        if (Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".permissions") == null) {
            player.sendMessage(ChatColor.RED + "Rank does not exist!");
            return false;
        }
        if (!(Bukkit.getOnlinePlayers().contains(Bukkit.getPlayer(args[0])))) {
            player.sendMessage(ChatColor.RED + "Player is not online!");
            return false;
        }

        ArrayList<String> members;

        if (!(Main._instance.getConfig().isSet("ranks." + args[1].toLowerCase() + ".members"))) {
            player.sendMessage(ChatColor.RED + "Rank has no members!");
            return false;
        }
        if (!(Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".members").contains(args[0]))){
            player.sendMessage(ChatColor.RED + "Player does not have this rank!");
            return false;
        }

        members = (ArrayList<String>) Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".members");
        members.remove(args[0]);
        Main._instance.getConfig().set("ranks." + args[1].toLowerCase() + ".members", members);
        Main._instance.saveConfig();
        player.sendMessage(ChatColor.DARK_AQUA + args[0] + " has been removed from the rank!");

        return true;
    }
}
