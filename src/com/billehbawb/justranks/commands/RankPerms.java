package com.billehbawb.justranks.commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.billehbawb.justranks.Main;

public class RankPerms implements CommandExecutor {
	
	@SuppressWarnings("unchecked")
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if (!(sender instanceof Player)){
			return false;
		}
		
		Player player = (Player) sender;
		
		if (args.length != 3){
			player.sendMessage(ChatColor.RED + "Incorrect usage! Try '/rankperms <add/del> <RankName> <PermissionNode>'");
			return false;
		}
		if (!(args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("del"))) {
			player.sendMessage(ChatColor.RED + "Incorrect usage! Try '/rankperms <add/del> <RankName> <PermissionNode>'");
			return false;
		}
		if (Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".permissions") == null){
			player.sendMessage(ChatColor.RED + "Rank " + args[1] + " does not exist!");
			return false;
		}
		
		if (args[0].equalsIgnoreCase("add")){
			if (Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".permissions").contains(args[2])){
				player.sendMessage(ChatColor.RED + "Rank " + args[1] + " already has access to this permission! (Did you mean to remove the permission?)");
				return false;
			} else {
				ArrayList<String> oldList = (ArrayList<String>) Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".permissions");
				oldList.add(args[2]);
				Main._instance.getConfig().set("ranks." + args[1].toLowerCase() + ".permissions", oldList);
				Main._instance.saveConfig();
				player.sendMessage(ChatColor.DARK_AQUA + "Permission added!");
				return true;
			}
		}
		
		if (args[0].equalsIgnoreCase("del")){
			if (!(Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".permissions").contains(args[2]))){
				player.sendMessage(ChatColor.RED + "Rank " + args[1] + " does not have access to this permission! (Did you mean to add the permission?)");
				return false;
			} else {
				ArrayList<String> oldList = (ArrayList<String>) Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".permissions");
				oldList.remove(args[2]);
				Main._instance.getConfig().set("ranks." + args[1].toLowerCase() + ".permissions", oldList);
				Main._instance.saveConfig();
				player.sendMessage(ChatColor.DARK_AQUA + "Permission removed!");
				return true;
			}
		}
			
		return false;
	}
}
