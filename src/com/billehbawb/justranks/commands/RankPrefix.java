package com.billehbawb.justranks.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.billehbawb.justranks.Main;

public class RankPrefix implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;

		if (args.length != 2) {
			player.sendMessage(ChatColor.RED
					+ "Incorrect usage! Try '/rankprefix <RankName> <Prefix>' (If you are having issues, please make the prefix without any spaces. A space will be added between the prefix and the player's name by default.)");
			return false;
		}
		if (Main._instance.getConfig().getList("ranks." + args[0].toLowerCase() + ".permissions") == null) {
			player.sendMessage(ChatColor.RED + "Rank does not exist!");
			return false;
		}

		Main._instance.getConfig().addDefault("ranks." + args[0].toLowerCase() + ".prefix", args[1]);
		Main._instance.saveConfig();
		player.sendMessage(ChatColor.DARK_AQUA + "Prefix set!");

		return false;
	}
}
