package com.billehbawb.justranks.commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.billehbawb.justranks.Main;

public class RankAdd implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if (!(sender instanceof Player)){
			return false;
		}
		
		Player player = (Player) sender;
		
		if (args.length != 1){
			player.sendMessage(ChatColor.RED + "Incorrect usage! Try '/rankadd <RankName>'");
			return false;
		}
		if (Main._instance.getConfig().getList("ranks." + args[0].toLowerCase() + ".permissions") != null){
			player.sendMessage(ChatColor.RED + "Rank already exists! Did you mean '/rankdel'?");
			return false;
		}
		
		ArrayList<String> defaultPermissions = new ArrayList<String>();
		
		defaultPermissions.add("makesure.thatthere");
		defaultPermissions.add("isalways.atleastone");
		defaultPermissions.add("permissionat.alltimes");
		
		Main._instance.getConfig().addDefault("ranks." + args[0].toLowerCase() + ".permissions", defaultPermissions);
		Main._instance.saveConfig();
		player.sendMessage(ChatColor.DARK_AQUA + "Rank " + args[0] + " has been created! (Edit this rank with '/rankperms' and '/rankprefix')");
		
		return true;
	}
}
