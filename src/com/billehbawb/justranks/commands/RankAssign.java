package com.billehbawb.justranks.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.billehbawb.justranks.Main;

public class RankAssign implements CommandExecutor {
	@SuppressWarnings("unchecked")
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;

		if (args.length != 2) {
			player.sendMessage(ChatColor.RED + "Incorrect usage! Try '/rankassign <PlayerName> <RankName>'");
			return false;
		}
		if (Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".permissions") == null) {
			player.sendMessage(ChatColor.RED + "Rank does not exist!");
			return false;
		}
		if (!(Bukkit.getOnlinePlayers().contains(Bukkit.getPlayer(args[0])))) {
			player.sendMessage(ChatColor.RED + "Player is not online!");
		}

		ArrayList<String> members = new ArrayList<String>();

		if (!(Main._instance.getConfig().isSet("ranks." + args[1].toLowerCase() + ".members"))) {
			members.add(args[0]);
			Main._instance.getConfig().addDefault("ranks." + args[1].toLowerCase() + ".members", members);
			Main._instance.saveConfig();
		} else {
            if (Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".members").contains(args[0])){
                player.sendMessage(ChatColor.RED + "Player already has this rank!");
                return false;
            }
			members = (ArrayList<String>) Main._instance.getConfig().getList("ranks." + args[1].toLowerCase() + ".members");
			members.add(args[0]);
			Main._instance.getConfig().set("ranks." + args[1].toLowerCase() + ".members", members);
			Main._instance.saveConfig();
		}
		player.sendMessage(ChatColor.DARK_AQUA + args[0] + " has been added to the rank!");

		return true;
	}
}
