package com.billehbawb.justranks.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.billehbawb.justranks.Main;

public class RankDel implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)){
			return false;
		}
		
		Player player = (Player) sender;
		
		if (args.length != 1){
			player.sendMessage(ChatColor.RED + "Incorrect usage! Try '/rankdel <RankName>'");
			return false;
		}
		if (Main._instance.getConfig().getList("ranks." + args[0].toLowerCase() + ".permissions") == null){
			player.sendMessage(ChatColor.RED + "Rank does not exist! Did you mean '/rankadd'?");
			return false;
		}
		
		Main._instance.getConfig().set("ranks." + args[0].toLowerCase(), null);
		Main._instance.saveConfig();
		Main._instance.reloadConfig();
		player.sendMessage(ChatColor.DARK_AQUA + "Rank " + args[0] + " has been removed!");
		
		return true;
	}
}