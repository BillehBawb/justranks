package com.billehbawb.justranks.events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.billehbawb.justranks.Main;

public class PrefixChat implements Listener {

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {

		Player player = event.getPlayer();

		for (String key : Main._instance.getConfig().getConfigurationSection("ranks").getKeys(false)) {
			if (Main._instance.getConfig().getList("ranks." + key + ".members").contains(player.getName())) {
				event.setFormat(ChatColor.translateAlternateColorCodes('&',
						Main._instance.getConfig().getString("ranks." + key + ".prefix")) + " " + player.getName()
						+ ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + event.getMessage());
			}
		}
	}
}
