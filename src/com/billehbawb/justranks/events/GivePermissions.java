package com.billehbawb.justranks.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;

import com.billehbawb.justranks.Main;

public class GivePermissions implements Listener {

	private HashMap<UUID, PermissionAttachment> permissions = new HashMap<UUID, PermissionAttachment>();

	@EventHandler
	@SuppressWarnings("unchecked")
	public void onPlayerJoin(PlayerJoinEvent event) {

		Player player = event.getPlayer();

		for (String key : Main._instance.getConfig().getConfigurationSection("ranks").getKeys(false)) {
			if (Main._instance.getConfig().getList("ranks." + key + ".members").contains(player.getName())) {

				ArrayList<String> permissionList = (ArrayList<String>) Main._instance.getConfig()
						.getList("ranks." + key + ".permissions");

                PermissionAttachment attachment = player.addAttachment(Main._instance);
                permissions.put(player.getUniqueId(), attachment);

                permissionList.forEach(permission -> addPermission(player, permission));
			}
		}
	}

    public void addPermission(Player p, String perm) {
    	permissions.get(p.getUniqueId()).setPermission(perm, true);
    }
}
