package com.billehbawb.justranks;

import com.billehbawb.justranks.commands.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.billehbawb.justranks.events.GivePermissions;
import com.billehbawb.justranks.events.PrefixChat;

public class Main extends JavaPlugin {
	
	public static Main _instance;
	
	public void onEnable() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		_instance = this;
		
		getCommand("rankadd").setExecutor(new RankAdd());
		getCommand("rankperms").setExecutor(new RankPerms());
		getCommand("rankprefix").setExecutor(new RankPrefix());
		getCommand("rankassign").setExecutor(new RankAssign());
		getCommand("rankrevoke").setExecutor(new RankRevoke());
		getCommand("rankdel").setExecutor(new RankDel());
		
		Bukkit.getPluginManager().registerEvents(new GivePermissions(), this);
		Bukkit.getPluginManager().registerEvents(new PrefixChat(), this);
	}
}
